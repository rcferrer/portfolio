<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link
        href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,700;1,400&family=Poppins:wght@700&family=Roboto&display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="./assets/css/style.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Home - RC's Portfolio</title>
</head>

<body>
    <div class="container-fill top-line-sub"></div>
    <div class="container-fill top-line"></div>

    <div class="container main-bg">

        <!-- <div class="main-view"> -->

        <header class="row">

            <?php include "./partials/navbar.php" ?>
        </header>

        <main>

            <div class="row jumbotron main pb-0">
                <div class="col-lg-6 col-md-12 col-sm-12 my-auto d-flex justify-content-center">
                    <img src="./assets/images/wom-p.png" alt="" class="image-main"></div>
                <div class="col-lg-6 col-md-12 col-sm-12 my-auto">
                    <h6>Full Stack Web Developer</h6>
                    <h1 class="name">R O Z E L Y N - C L A I R E <br> F E R R E R</h1>
                    <p class="my-0"> A full stack web developer loves solving problems, </p>
                    <p>learning new technologies and
                        improving lives</p>
                    <div class="py-2">
                        <a href="#contact-section" class="cta contact-me">CONTACT ME</a>
                        <a href="https://www.linkedin.com/in/rozelyn-claire-ferrer/" class="cta view-resume"
                            target="_blank">VIEW
                            LINKEDIN</a>

                    </div>
                </div>
            </div>

            <hr>

            <?php include "./pages/about.php" ?>

            <hr>

            <?php include "./pages/portfolio.php" ?>

            <hr>

            <?php include "./pages/contact.php" ?>


        </main>



        <!-- </div> -->
    </div>

    <footer class="px-3">
        <?php include "./partials/footer.php" ?>
    </footer>

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
</body>

</html>