<nav class="navbar navbar-expand-lg navbar-light  col-12"> <img src="./../assets/images/initial.png" alt="R"
        class="logo">
    <a class="navbar-brand" href="#">RC Ferrer</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="#about-me-section">About</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./../pages/resume">Resume</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#portfolio-section">Portfolio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact-section">Contact</a>
            </li>
        </ul>
    </div>
</nav>