<div>
    <a href="https://www.linkedin.com/in/rozelyn-claire-ferrer/" target="_blank"><i
            class="footer-icon fab fa-linkedin"></i></a>
    <a href="https://gitlab.com/rcferrer" target="_blank"><i class="footer-icon fab fa-gitlab"></i></a>
    <a href="https://github.com/rcferrer" target="_blank"><i class="footer-icon fab fa-github"></i></a>
</div>

Copyright &copy; 2020 Rozelyn-Claire C. Ferrer